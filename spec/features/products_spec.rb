RSpec.feature "Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon)   { create(:taxon) }

  before do
    visit potepan_product_path(product.id)
  end

  it "商品名、商品価格が表示されている" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end

  it "一覧ページクリック後、各カテゴリーページが表示されている" do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
end
