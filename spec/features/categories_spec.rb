RSpec.feature "Category Features", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
  let(:other_taxon) { create(:taxon, name: "other_taxon") }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, taxons: [other_taxon]) }

  describe "GET potepan_category_path" do
    before do
      visit potepan_category_path(taxon.id)
    end

    it "Homeをクリックするとホーム画面が表示されている" do
      within ".breadcrumb" do
        click_on "HOME"
      end
      expect(current_path).to eq potepan_path
    end

    it "適切なtaxonが表示されている" do
      expect(page).to have_selector 'h2', text: taxon.name
    end

    it "カテゴリー一覧にtaxon名が表示されている" do
      within ".side-nav" do
        expect(page).to have_content taxon.name
      end
    end

    it "taxonomy,taxonに紐づいていないproductは表示されない" do
      within ".side-nav" do
        click_on taxonomy.name, match: :first
        expect(page).to have_content taxonomy.name
        click_on taxon.name, match: :first
        expect(page).to have_content taxon.name
      end
      expect(page).not_to have_link(other_taxon.name, href: potepan_category_path(other_taxon.id))
    end

    it "カテゴリーに紐づいていない商品名、値段は表示されない" do
      within ".productBox" do
        expect(current_path).not_to have_content other_product.name
        expect(current_path).not_to have_content other_product.display_price
        expect(current_path).not_to have_content other_product.description
      end
    end

    it "productをクリックしてページ遷移できる" do
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
