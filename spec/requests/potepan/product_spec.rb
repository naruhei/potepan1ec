RSpec.describe "Potepan::Products", type: :request do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before { get potepan_product_path product.id }

    it 'リクエストが成功することを確認' do
      expect(response.status).to eq 200
    end

    it '商品名が表示されていること' do
      expect(response.body).to include product.name
    end

    it '商品の値段が表示されていること' do
      expect(response.body).to include product.display_price.to_s
    end

    it '商品の説明文が表示されていること' do
      expect(response.body).to include product.description
    end
  end
end
