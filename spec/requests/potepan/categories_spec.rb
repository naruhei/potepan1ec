RSpec.describe "Categories", type: :request do
  let(:taxon) { create(:taxon) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe 'GET/potepan/categories/id' do
    before do
      get potepan_category_path(taxon.id)
    end

    it 'リクエストが成功することを確認' do
      expect(response).to have_http_status(200)
    end

    it 'taxonが表示されている' do
      expect(response.body).to include taxon.name
    end

    it 'taxonomyが表示されている' do
      expect(response.body).to include taxonomy.name
    end

    it '商品名が表示されている' do
      expect(response.body).to include product.name
    end

    it '商品の価格が表示されている' do
      expect(response.body).to include product.display_price.to_s
    end
  end
end
